Vexillum
==========

About
-----
#### Version
![release](https://gitlab.com/3fiftynine/public/vexillum/-/badges/release.svg)

#### Support Status
*Vexillum* is actively used and maintained.

#### Description
*Vexillum* is a PHPCS extension for 3Fiftynine coding standards in PHP-based projects.

With this package, it is possible to apply the extended coding standards to the entire product suite in a standardized manner.

#### Technical Requirements
*Vexillum* requires:

- PHP 8.1+

Installation
------------
#### Step 1: Composer
Start off by opening your command-line and run the following command inside your project:

```console
composer require bannister/vexillum
```

#### Step 2: Scripting
Once the package has been installed, add the following lines to the `scripts` section in your project's `composer.json`.

```json
"scripts": {
    "vexillum": "./vendor/bin/phpcs --standard=vendor/bannister/vexillum/Standards/ThreeFiftynine src/ -p",
    "vexillum-fix" : "php ./vendor/bin/phpcbf"
}
```
*Vexillum* is now ready for use in your project!

Usage
-----
Fortunately, using *Vexillum* is simple.
In order to check the coding standards in your project, simply execute the following:

```console
composer vexillum
```

Some coding standards violations can be fixed automatically via:

```console
composer vexillum-fix
```

Customization
-------------
Due to the installation instruction above, *Vexillum* will only check your `src` directory by default.
It is possible to alter the target directory by changing the final part of the `vexillum` script in your `composer.json`.

For example:
```json
"scripts": {
    "vexillum": "./vendor/bin/phpcs --standard=vendor/bannister/vexillum/Standards/ThreeFiftynine path/to/your/directory",
    "vexillum-fix" : "php ./vendor/bin/phpcbf"
}
```

Authors
-------
| Name        |      Function      | Contact             |
|-------------|--------------------|---------------------|
| Jacco Engel | Back-end Developer | Jacco@3Fiftynine.nl |
| Tom Peters  | Back-end Developer | Tom@3Fiftynine.nl   |
