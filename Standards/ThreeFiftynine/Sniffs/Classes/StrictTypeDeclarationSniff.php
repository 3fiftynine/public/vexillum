<?php

declare(strict_types=1);

namespace Vexillum\Standards\ThreeFiftynine\Sniffs\Classes;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vexillum\Standards\ThreeFiftynine\Helper\SnifferHelper;

/**
 * Sniffs whether the class declares its use of strict types.
 */
final class StrictTypeDeclarationSniff implements Sniff
{
    /**
     * @return int[]
     *
     * @link https://www.php.net/manual/en/tokens.php
     */
    final public function register(): array
    {
        return [T_CLASS];
    }

    public function process(File $phpcsFile, $stackPtr): void
    {
        $helper = new SnifferHelper($phpcsFile->path);

        $strictTypesLines = $helper->findLines('declare(strict_types=1);');
        $namespaceLines = $helper->findLines('namespace');

        $hasStrictTypes = empty($strictTypesLines) === false;
        $strictTypesPrecedeNamespace = array_key_first($strictTypesLines) < array_key_first($namespaceLines);

        if ($hasStrictTypes === false || $strictTypesPrecedeNamespace === false) {
            $error = "The class must declare strict typing before the namespace.";

            $phpcsFile->addError($error, $stackPtr, '');
        }
    }
}
