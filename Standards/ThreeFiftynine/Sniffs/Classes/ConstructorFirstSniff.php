<?php

declare(strict_types=1);

namespace Vexillum\Standards\ThreeFiftynine\Sniffs\Classes;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vexillum\Standards\ThreeFiftynine\Helper\SnifferHelper;

/**
 * Sniffs whether the constructor, if present, is the first function to be defined in a class.
 */
final class ConstructorFirstSniff implements Sniff
{
    /**
     * @return int[]
     *
     * @link https://www.php.net/manual/en/tokens.php
     */
    final public function register(): array
    {
        return [T_CLASS];
    }

    public function process(File $phpcsFile, $stackPtr): void
    {
        $helper = new SnifferHelper($phpcsFile->path);

        if ($helper->hasConstructor() === true) {
            $functions = $helper->getAllFunctions();
            $firstFunction = array_shift($functions);

            if ($firstFunction !== "__construct") {
                $error = "The constructor should be the first function in the class.";

                $phpcsFile->addError($error, $stackPtr, '');
            }
        }
    }
}
