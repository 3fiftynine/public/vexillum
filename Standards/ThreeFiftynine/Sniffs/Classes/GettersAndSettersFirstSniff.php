<?php

declare(strict_types=1);

namespace Vexillum\Standards\ThreeFiftynine\Sniffs\Classes;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vexillum\Standards\ThreeFiftynine\Helper\SnifferHelper;

/**
 * Sniffs whether getter and setter functions precede any other functions in the class.
 */
final class GettersAndSettersFirstSniff implements Sniff
{
    /**
     * @return int[]
     *
     * @link https://www.php.net/manual/en/tokens.php
     */
    final public function register(): array
    {
        return [T_CLASS];
    }

    public function process(File $phpcsFile, $stackPtr): void
    {
        $helper = new SnifferHelper($phpcsFile->path);

        if ($helper->isController() === false) {
            $gettersAndSetters = $helper->getGettersSetters();
            $nonGettersAndSetters = $helper->getNonGettersSetters();

            $lastGetterSetter = array_key_last($gettersAndSetters);
            $firstNonGetterSetter = array_key_first($nonGettersAndSetters);

            if ($firstNonGetterSetter > 0 && $lastGetterSetter > $firstNonGetterSetter) {
                $error = 'Define getter and setter functions before any other functions (excluding the constructor).';

                $phpcsFile->addError($error, $stackPtr, '');
            }
        }
    }
}
