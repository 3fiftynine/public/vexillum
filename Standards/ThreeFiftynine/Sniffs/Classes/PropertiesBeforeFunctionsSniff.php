<?php

declare(strict_types=1);

namespace Vexillum\Standards\ThreeFiftynine\Sniffs\Classes;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vexillum\Standards\ThreeFiftynine\Enum\Property;
use Vexillum\Standards\ThreeFiftynine\Helper\SnifferHelper;

/**
 * Sniffs whether a class' properties are defined before its functions.
 */
final class PropertiesBeforeFunctionsSniff implements Sniff
{
    /**
     * @return int[]
     *
     * @link https://www.php.net/manual/en/tokens.php
     */
    final public function register(): array
    {
        return [T_CLASS];
    }

    public function process(File $phpcsFile, $stackPtr): void
    {
        $helper = new SnifferHelper($phpcsFile->path);

        $nonStaticProperties = $helper->getProperties(Property::NonStatic);
        $staticProperties = $helper->getProperties(Property::Static);
        $functions = $helper->getAllFunctions();

        $lastNonStaticProperty = array_key_last($nonStaticProperties);
        $lastStaticProperty = array_key_last($staticProperties);
        $firstFunction = array_key_first($functions);

        if (count($functions) > 0 && $lastNonStaticProperty > $firstFunction && $lastStaticProperty > $firstFunction) {
            $error = 'Define a class\' properties before its functions.';

            $phpcsFile->addError($error, $stackPtr, '');
        }
    }
}
