<?php

declare(strict_types=1);

namespace Vexillum\Standards\ThreeFiftynine\Sniffs\Classes;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vexillum\Standards\ThreeFiftynine\Helper\SnifferHelper;

/**
 * Sniffs a getter-setter pair of functions to determine whether the getter precedes the setter function.
 */
final class GettersBeforeSettersSniff implements Sniff
{
    /**
     * @return int[]
     *
     * @link https://www.php.net/manual/en/tokens.php
     */
    final public function register(): array
    {
        return [T_CLASS];
    }

    public function process(File $phpcsFile, $stackPtr): void
    {
        $helper = new SnifferHelper($phpcsFile->path);

        $functions = $helper->getGettersSetters();

        foreach ($functions as $function) {
            $target = substr($function, 3);

            $getterFunction = 'get' . $target;
            $setterFunction = 'set' . $target;

            $getterLineNumber = array_search($getterFunction, $functions);
            $setterLineNumber = array_search($setterFunction, $functions);

            unset($functions[$getterLineNumber]);
            unset($functions[$setterLineNumber]);

            if ($getterLineNumber !== false && $setterLineNumber !== false && $getterLineNumber > $setterLineNumber) {
                $error = $getterFunction . ' should precede ' . $setterFunction . '.';

                $phpcsFile->addError($error, $stackPtr, '', '');
            }
        }
    }
}
