<?php

declare(strict_types=1);

namespace Vexillum\Standards\ThreeFiftynine\Sniffs\Classes;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vexillum\Standards\ThreeFiftynine\Enum\Property;
use Vexillum\Standards\ThreeFiftynine\Helper\SnifferHelper;

/**
 * Sniffs whether the default values for class properties are set inline, rather than in the constructor.
 */
final class PropertyDefaultInConstructorSniff implements Sniff
{
    /**
     * @return int[]
     *
     * @link https://www.php.net/manual/en/tokens.php
     */
    final public function register(): array
    {
        return [T_CLASS];
    }

    public function process(File $phpcsFile, $stackPtr): void
    {
        $helper = new SnifferHelper($phpcsFile->path);

        $allProperties = $helper->getProperties(Property::NonStatic);
        $noDefaults = $helper->getProperties(Property::NonStatic, false);

        if ($allProperties !== $noDefaults) {
            $error = 'Default values of class properties should be set in the constructor.';

            $phpcsFile->addError($error, $stackPtr, '');
        }
    }
}
