<?php

declare(strict_types=1);

namespace Vexillum\Standards\ThreeFiftynine\Sniffs\Variable;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use PHP_CodeSniffer\Util\Common;

/**
 * Sniffs whether a variable name is formatted as camelCase.
 */
final class VariableCamelCaseSniff implements Sniff
{
    /**
     * @return int[]
     *
     * @link https://www.php.net/manual/en/tokens.php
     */
    final public function register(): array
    {
        return [T_VARIABLE];
    }

    public function process(File $phpcsFile, $stackPtr): void
    {
        $tokens = $phpcsFile->getTokens();
        $variable = $tokens[$stackPtr]['content'];

        if (str_starts_with($variable, '$') === true) {
            $variable = substr($variable, 1);
        }

        $excludedVarNames = ['_SERVER', '_POST', '_GET', '_ENV'];
        $isRegularVariable = in_array($variable, $excludedVarNames) === false;
        $isCamelCase = Common::isCamelCaps($variable);
        if ($isRegularVariable === true && $isCamelCase === false) {
            $error = 'Variable (' . $variable . ') did not match expected camelCase format.';

            $phpcsFile->addError($error, $stackPtr, '', '');
        }
    }
}
