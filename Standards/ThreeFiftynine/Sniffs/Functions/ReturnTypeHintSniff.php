<?php

declare(strict_types=1);

namespace Vexillum\Standards\ThreeFiftynine\Sniffs\Functions;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vexillum\Standards\ThreeFiftynine\Helper\SnifferHelper;

/**
 * Sniffs whether a function has a return type specified.
 */
final class ReturnTypeHintSniff implements Sniff
{
    /**
     * @return int[]
     *
     * @link https://www.php.net/manual/en/tokens.php
     */
    final public function register(): array
    {
        return [T_FUNCTION];
    }

    public function process(File $phpcsFile, $stackPtr): void
    {
        $helper = new SnifferHelper($phpcsFile->path);

        $tokens = $phpcsFile->getTokens();
        $lineNumber = $tokens[$stackPtr]['line'];
        $functionComponents = $helper->getFunctionComponents($lineNumber);

        $functionName = $functionComponents[5];
        $returnType = $functionComponents[7] ?? null;

        if ($functionName !== '__construct' && $returnType === null) {
            $error = 'Function (' . $functionName . ') lacks a return type. Providing a return type is strongly recommended.';

            $phpcsFile->addWarning($error, $stackPtr, '');
        }
    }
}
