<?php

declare(strict_types=1);

namespace Vexillum\Standards\ThreeFiftynine\Sniffs\Functions;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vexillum\Standards\ThreeFiftynine\Helper\SnifferHelper;

/**
 * Sniffs whether a function's parameters are properly type-hinted.
 */
final class ParameterTypeHintSniff implements Sniff
{
    /**
     * @return int[]
     *
     * @link https://www.php.net/manual/en/tokens.php
     */
    final public function register(): array
    {
        return [T_FUNCTION];
    }

    public function process(File $phpcsFile, $stackPtr): void
    {
        $helper = new SnifferHelper($phpcsFile->path);

        $tokens = $phpcsFile->getTokens();
        $lineNumber = $tokens[$stackPtr]['line'];

        $parameters = $helper->getFunctionParameters($lineNumber);
        foreach ($parameters as $parameter) {
            $hasParameter = $parameter !== '';
            $typeAndName = explode(' ', $parameter);

            if ($hasParameter === true && count($typeAndName) < 2) {
                $error = 'Parameter (' . $parameter . ') lacks a type-hint. Type-hinting is strongly recommended.';

                $phpcsFile->addWarning($error, $stackPtr, '');
            }
        }
    }
}
