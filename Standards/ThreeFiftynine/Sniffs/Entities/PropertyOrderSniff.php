<?php

declare(strict_types=1);

namespace Vexillum\Standards\ThreeFiftynine\Sniffs\Entities;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vexillum\Standards\ThreeFiftynine\Enum\Property;
use Vexillum\Standards\ThreeFiftynine\Helper\SnifferHelper;

/**
 * Sniffs whether the Doctrine entity class has its properties in the expected order.
 */
final class PropertyOrderSniff implements Sniff
{
    /**
     * @return int[]
     *
     * @link https://www.php.net/manual/en/tokens.php
     */
    final public function register(): array
    {
        return [T_CLASS];
    }

    public function process(File $phpcsFile, $stackPtr): void
    {
        $helper = new SnifferHelper($phpcsFile->path);

        if ($helper->isEntity() === true) {
            $properties = $helper->getProperties(Property::NonStatic);

            $firstPropertyLine = array_key_first($properties);
            $firstProperty = $properties[$firstPropertyLine];
            if (in_array('id', $properties) === true && $firstProperty !== 'id') {
                $error = 'An entity\'s first property should be its id.';

                $phpcsFile->addError($error, $stackPtr, '');
            }
        }
    }
}
