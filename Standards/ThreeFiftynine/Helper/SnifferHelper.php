<?php

declare(strict_types=1);

namespace Vexillum\Standards\ThreeFiftynine\Helper;

use Vexillum\Standards\ThreeFiftynine\Enum\Property;

/**
 * Provides functionality that aids the code sniffs fulfill their purpose in an efficient and readable way.
 */
class SnifferHelper
{
    /**
     * A list of text lines, referred to as the file.
     *
     * @var string[]
     */
    private array $lines;

    public function __construct(string $fileName)
    {
        $this->lines = file($fileName);
    }

    /***********/
    /* UTILITY */
    /***********/

    /**
     * Retrieves a single line's content based on its line number.
     */
    public function getLine(int $lineNumber): string
    {
        $index = $lineNumber - 1;
        $line = $this->lines[$index];

        return trim($line);
    }

    /**
     * Finds all occurrences of the provided search term in the file and returns a list of lines by their line numbers.
     *
     * @return string[]
     */
    public function findLines(string $searchTerm): array
    {
        $lines = $this->lines;

        return array_filter($lines, function ($line) use ($searchTerm) {
            return str_contains($line, $searchTerm);
        });
    }

    /**************/
    /* FILE CLASS */
    /**************/

    /**
     * Searches the file's namespace for the provided search term.
     */
    public function isInNamespace(string $search): bool
    {
        $namespaceLines = $this->findLines('namespace');

        $lineNumber = array_key_first($namespaceLines);
        $line = $namespaceLines[$lineNumber];

        $start = strlen('namespace ');
        $end = strpos($line, ';') - $start;

        $namespace = strtolower(substr($line, $start, $end));
        $fragments = explode('\\', $namespace);

        return in_array($search, $fragments) === true;
    }

    /**
     * Checks whether the file is a controller, based on namespace.
     */
    public function isController(): bool
    {
        return $this->isInNamespace('controller');
    }

    /**
     * Checks whether the file is a Doctrine entity, based on namespace.
     */
    public function isEntity(): bool
    {
        return $this->isInNamespace('entity');
    }

    /**
     * Checks whether the file is a model class.
     */
    public function isModel(): bool
    {
        return $this->isInNamespace('model');
    }

    /**
     * Breaks down a property declaration into its separate components.
     *
     * Indices:
     *     0: full property declaration
     *     1: public, private, or protected
     *     2: static or readonly
     *     3: type
     *     4: name
     *     5: default value
     *
     * @return string[]
     */
    public function getPropertyComponents(int $lineNumber, Property $propertyType, bool $includeDefaultValue = true): array
    {
        $regex = match($propertyType) {
            Property::NonStatic => '/^(public|private|protected) (?!static )(readonly )?[?]?([a-zA-Z]+ )?\$([a-zA-Z0-9-_]+)(( = )([^;]+))?;$/',
            Property::Static => '/^(public|private|protected) (static )[?]?([a-zA-Z]+ )?\$([a-zA-Z0-9-_]+)(( = )([^;]+))?;$/',
        };

        if ($includeDefaultValue === false) {
            $regex = str_replace('(( = )([^;]+))?', '', $regex);
        }

        $line = $this->getLine($lineNumber);
        preg_match($regex, $line, $components);

        if ($includeDefaultValue === true && empty($components) === false && count($components) === 8) {
            $components[5] = $components[7];
            $components = array_slice($components, 0, 6);
        }

        return $components;
    }

    /**
     * Get a class' properties, whether public, private, or protected.
     *
     * @return string[]
     */
    public function getProperties(Property $propertyType, bool $includeDefaultValue = true): array
    {
        $properties = [];

        $lines = $this->lines;
        foreach ($lines as $lineIndex => $line) {
            $lineNumber = $lineIndex + 1;
            $propertyComponents = $this->getPropertyComponents($lineNumber, $propertyType, $includeDefaultValue);
            if (empty($propertyComponents) === false) {
                $properties[$lineIndex] = $propertyComponents[4];
            }
        }

        return $properties;
    }

    /**
     * Retrieves a list of docblocks in the file, each as a separate list with entries for each line.
     *
     * @return string[][]
     */
    public function getDocBlocks(): array
    {
        $docBlocks = [];

        $start = false;
        foreach ($this->lines as $lineNumber => $line) {
            $line = trim($line);

            if ($line === '/**') {
                $docBlock = [];
                $docBlock[] = $line;
                $startingLineNumber = $lineNumber;
                $start = true;
            }

            if ($start === true) {
                $docBlock[$lineNumber] = $line;

                if ($line === '*/') {
                    $docBlocks[$startingLineNumber] = $docBlock;
                    $start = false;
                }
            }
        }

        return $docBlocks;
    }

    /******************/
    /* FILE FUNCTIONS */
    /******************/

    /**
     * Breaks down a function declaration into its separate components.
     *
     * Indices:
     *     0: full function declaration
     *     1: final or abstract
     *     2: public, private, or protected
     *     3: static
     *     4: function
     *     5: function name
     *     6: parameters
     *     7: return type
     *
     * @return string[]
     */
    public function getFunctionComponents(int $lineNumber): array
    {
        $function = $this->getLine($lineNumber);

        $isMultilineFunction = str_ends_with($function, '(');
        if ($isMultilineFunction === true && (strpos($function, 'function') !== false || strpos($function, '=>') !== false)) {
            $functionLines = [];

            do {
                $currentLine = $this->getLine($lineNumber);
                $functionLines[] = $currentLine;
                $lineNumber++;
            } while (str_contains($currentLine, '{') === false);

            $lastLineKey = array_key_last($functionLines);
            $functionLines[$lastLineKey] = str_replace(' {', '', $functionLines[$lastLineKey]);

            $function = implode(' ', $functionLines);
        }

        preg_match('/^(final |abstract )?(public |private |protected )(static )?(function )([a-zA-Z0-9-_]+)\((.*)\)(: [?]?[a-zA-Z |]+)?$/', $function, $components);

        if (array_key_exists(7, $components) === true) {
            $components[7] = str_replace(': ', '', $components[7]);
        }

        return array_map('trim', $components);
    }

    /**
     * Generates a list of all function names present in the file.
     *
     * @return string[]
     */
    public function getAllFunctions(): array
    {
        $functions = [];

        foreach ($this->lines as $lineKey => $line) {
            $lineNumber = $lineKey + 1;

            $function = $this->getFunctionComponents($lineNumber);
            if (empty($function) === false) {
                $functionName = $function[5];

                $functions[$lineNumber] = $functionName;
            }
        }

        return $functions;
    }

    /**
     * Checks whether the file has a constructor.
     */
    public function hasConstructor(): bool
    {
        $functions = $this->getAllFunctions();

        return in_array("__construct", $functions);
    }

    /**
     * Retrieves a function's parameters.
     *
     * @return string[]
     */
    public function getFunctionParameters(int $lineNumber): array
    {
        $functionComponents = $this->getFunctionComponents($lineNumber);

        $parameters = array_key_exists(6, $functionComponents) === true ? explode(',', $functionComponents[6]) : [];

        return array_map('trim', $parameters);
    }

    /**
     * Generates a list of getter and setter functions, indexed by their line numbers.
     *
     * @return string[]
     */
    public function getGettersSetters(): array
    {
        $gettersSetters = [];

        $functions = $this->getAllFunctions();
        foreach ($functions as $lineNumber => $functionName) {
            if ($this->isEntity() === true || $this->isModel() === true) {
                $regex = '/^((get)|(set)|(is)|(add)|(remove))[A-Z]/';
            } else {
                $regex = '/^((get)|(set))[A-Z]/';
            }

            if (preg_match($regex, $functionName) === 1) {
                $gettersSetters[$lineNumber] = $functionName;
            }
        }

        return $gettersSetters;
    }

    /**
     * Generates a list of functions, indexed by their line numbers, excluding getters and setters.
     *
     * @return string[]
     */
    public function getNonGettersSetters(bool $includeConstructor = false): array
    {
        $nonGettersSetters = [];

        $functions = $this->getAllFunctions();
        foreach ($functions as $lineNumber => $functionName) {
            if ($this->isEntity() === true || $this->isModel() === true) {
                $regex = '/^((get)|(set)|(is)|(add)|(remove))[A-Z]/';
            } else {
                $regex = '/^((get)|(set))[A-Z]/';
            }

            if (preg_match($regex, $functionName) === 0) {
                if ($functionName === '__construct' && $includeConstructor === false) {
                    continue;
                }

                $nonGettersSetters[$lineNumber] = $functionName;
            }
        }

        return $nonGettersSetters;
    }

    /**
     * Retrieves a docblock associated with a function line-by-line.
     *
     * @return string[]
     */
    public function getDocBlockForFunction(int $lineNumber): array
    {
        $docBlock = [];

        $line = $this->getLine($lineNumber);
        while ($lineNumber > 0 && $line != "/**" && $line != "}") {
            $docBlock[] = $line;

            $lineNumber--;
            $line = $this->getLine($lineNumber);
        }

        array_shift($docBlock);

        return $docBlock;
    }
}
