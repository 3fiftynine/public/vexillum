<?php

declare(strict_types=1);

namespace Vexillum\Standards\ThreeFiftynine\Test;

/**
 * This class can be used to test out sniffs using the `composer vexillum-test` command.
 */
class Sandbox
{
    public function testMethod(): void
    {
    }
}
